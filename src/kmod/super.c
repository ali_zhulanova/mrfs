#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/buffer_head.h>


#include "mrfs.h"


static struct kmem_cache * mrfs_inode_cachep;

void mrfs_msg(struct super_block *sb, const char *function,
	      const char *fmt, ...)
{
	struct va_format vaf;
	va_list args;

	va_start(args, fmt);


	vaf.fmt = fmt;
	vaf.va = &args;

	printk(KERN_INFO "MRFS (%s): message: %s: %pV\n",
	       sb->s_id, function, &vaf);

	va_end(args);
}


/* TODO: add the setting error flag to sb in mrfs_err() */
void mrfs_err(struct super_block *sb, const char *function,
              const char *fmt, ...)
{
	struct va_format vaf;
	va_list args;

	va_start(args, fmt);


	vaf.fmt = fmt;
	vaf.va = &args;

	printk(KERN_CRIT "MRFS (%s): error: %s: %pV\n",
	       sb->s_id, function, &vaf);

	va_end(args);
}



static void mrfs_put_super (struct super_block * sb)
{
	struct mrfs_sb_info *sbi = MRFS_SB(sb);



	sb->s_fs_info = NULL;
	kfree(sbi);
}

static int mrfs_statfs (struct dentry * dentry, struct kstatfs * buf)
{

	return 0;
}




static const struct super_operations mrfs_super_ops = {
	/* .alloc_inode	= mrfs_alloc_inode, */
	/* .destroy_inode	= mrfs_destroy_inode, */
	/* .write_inode	= mrfs_write_inode, */
	/* .evict_inode	= mrfs_evict_inode, */
	.put_super	= mrfs_put_super,
	/* .sync_fs	= mrfs_sync_fs, */
	/* .freeze_fs	= mrfs_freeze, */
	/* .unfreeze_fs	= mrfs_unfreeze, */
	/* .statfs		= mrfs_statfs, */
	.statfs		= simple_statfs,
	/* .remount_fs	= mrfs_remount, */
	/* .show_options	= mrfs_show_options, */
/* #ifdef CONFIG_QUOTA */
/*         .quota_read	= mrfs_quota_read, */
/*         .quota_write	= mrfs_quota_write, */
/*         .get_dquots	= mrfs_get_dquots, */
/* #endif */
	.drop_inode     = generic_delete_inode, /* Not needed, is the default */
};




static void init_once(void *foo)
{
	struct mrfs_inode_info *mi = (struct mrfs_inode_info *) foo;

	rwlock_init(&mi->i_meta_lock);

	inode_init_once(&mi->vfs_inode);
}

static int __init init_inodecache(void)
{
	mrfs_inode_cachep = kmem_cache_create("mrfs_inode_cache",
					     sizeof(struct mrfs_inode_info),
					     0, (SLAB_RECLAIM_ACCOUNT|
						SLAB_MEM_SPREAD|SLAB_ACCOUNT),
					     init_once);
	if (mrfs_inode_cachep == NULL)
		return -ENOMEM;
	return 0;
}


static void destroy_inodecache(void)
{
	/*
	 * Make sure all delayed rcu free inodes are flushed before we
	 * destroy cache.
	 */
	rcu_barrier();
	kmem_cache_destroy(mrfs_inode_cachep);
	mrfs_inode_cachep = NULL;
}


static int mrfs_fill_super(struct super_block *sb, void *data, int silent)
{
	struct buffer_head * bh;
	struct mrfs_super_block * ms;
	struct mrfs_sb_info * sbi;
	struct inode *root = NULL;
	int blocksize = BLOCK_SIZE;
	/* int ret = -EINVAL; */
	int ret = 0;

	sbi = kzalloc(sizeof(*sbi), GFP_KERNEL);
	if (!sbi)
		goto failed;

	sb->s_fs_info = sbi;

	/* TODO: add spinlock if needed */


	blocksize = sb_min_blocksize(sb, BLOCK_SIZE);
	if (!blocksize) {
		mrfs_err(sb, "mrfs_fill_super", "error: unable to set blocksize");
		goto failed_sbi;
	}


	if (!(bh = sb_bread(sb, 0))) {
		mrfs_err(sb, "mrfs_fill_super", "error: unable to read superblock");
		goto failed_sbi;
	}

	ms = (struct mrfs_super_block *)bh->b_data;
	sbi->s_ms = ms;

	ms->s_magic = sb->s_magic = le16_to_cpu(ms->s_magic);
	sb->s_fs_info = sbi;

	if (sb->s_magic != MRFS_SUPER_MAGIC) {
		mrfs_err(sb, "mrfs_fill_super", "Magic number missmatch\n");
		/* Temporary disabled */
		/* ret = -EINVAL; */
		/* goto magic_mismatch; */
	}




	sb->s_op = &mrfs_super_ops;
	root = new_inode(sb);
	if (!root) {
		mrfs_msg(sb, KERN_ERR, "inode allocation error\n");
		/* add goto */
		ret = -ENOMEM;
	}
	root->i_ino = 0;
	root->i_sb = sb;
	root->i_atime = root->i_mtime = root->i_ctime = CURRENT_TIME;
	inode_init_owner(root, NULL, S_IFDIR);

	sb->s_root = d_make_root(root);
	if (!sb->s_root) {
		mrfs_msg(sb, "mrfs_fill_super", "root creation failed\n");
		ret = -ENOMEM;
	}




	ret = 0;
magic_mismatch:
	/* mrfs_msg(sb, KERN_ERR, */
	/*         "error: can't find an mrfs filesystem on dev %s.", */
	/*         sb->s_id); */
	goto failed_mount;
failed_mount:
	brelse(bh);
failed_sbi:
	sb->s_fs_info = NULL;
	kfree(sbi);
failed:
	return ret;
}


static struct dentry *mrfs_mount(struct file_system_type *fs_type, int flags,
		       const char *dev_name, void *data)
{
	struct dentry *const entry = mount_bdev(fs_type, flags, dev_name,
						data, mrfs_fill_super);

	if (IS_ERR(entry));

	return entry;
}


static struct file_system_type mrfs_type = {
	.owner		= THIS_MODULE,
	.name		= "mrfs",
	.mount		= mrfs_mount,
	.kill_sb	= kill_block_super,
	.fs_flags	= FS_REQUIRES_DEV,
};
MODULE_ALIAS_FS("mrfs");

static int __init mrfs_init(void)
{
	int err;

	err = init_inodecache();
	if (err) {
		/* mrfs_msg(""); */
		goto out1;
	}

	err = register_filesystem(&mrfs_type);
	if (err) {
		/* mrfs_msg(""); */
		goto out2;
	}

	return 0;
out2:
	destroy_inodecache();
out1:
	return err;
}

static void __exit mrfs_exit(void)
{
	int err;

	err = unregister_filesystem(&mrfs_type);
	if (err) {
		/* mrfs_msg(""); */
	}

}


MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Mister Filesystem");
MODULE_AUTHOR("Alina Zhulanova");
module_init(mrfs_init);
module_exit(mrfs_exit);
