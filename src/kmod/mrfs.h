#ifndef MRFS_H
#define MRFS_H

#define MRFS_SUPER_MAGIC 0x666

/*
 * Debug code
 */
#ifdef EXT2FS_DEBUG
#	define mrfs_debug(f, a...)	{ \
					printk ("MRFS DEBUG (%s, %d): %s:", \
						__FILE__, __LINE__, __func__); \
				  	printk (f, ## a); \
					}
#else
#	define mrfs_debug(f, a...)	/**/
#endif


static inline struct mrfs_sb_info *MRFS_SB(struct super_block *sb)
{
	return sb->s_fs_info;
}


/*
 * Structure of the super block
 */
struct mrfs_super_block {
	__le16	s_magic;		/* Magic signature */
};



/*
 * mrfs super-block data in memory
 */
struct mrfs_sb_info {
	/* TODO: add nls */
	struct mrfs_super_block * s_ms;	/* Pointer to the mrfs super block */
};




/*
 * mrfs inode data in memory
 */
struct mrfs_inode_info {



	rwlock_t i_meta_lock;

	struct inode	vfs_inode;
};


#endif /* MRFS_H */
