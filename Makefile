
.PHONY: all kmod utils clean

all:
	cd src && $(MAKE) all

kmod:
	cd src && $(MAKE) kmod

utils:
	cd src && $(MAKE) utils

clean:
	cd src && $(MAKE) clean
	rm -f build/kmod/mrfs.ko
