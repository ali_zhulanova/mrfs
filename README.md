# MRFS ( Mr. File System )

This project creates to get some knowledge in Linux kernel development.

## Getting Started

It's better to test on Virtual Machine with linux kernel 4.x.
This project contains two parts. One of them is kernel module with a file system and another one is utils for it.

### Prerequisites

For Ubuntu

```
sudo apt-get install gcc build-essential
```

```
sudo apt-get install linux-headers-`uname -r`
```

For Arch

```
pacman -S base-devel linux-headers
```

### Installing

To build only kernel module of file system you will need

```
make kmod
```

To build only utils for file system you will need

```
make utils
```

To build both you will need

```
make all
```

Add MRFS to kernel:

```
sudo insmod ./build/kmod/mrfs.ko
```

Remove MRFS from kernel:

```
sudo rmmod mrfs
```

Check that MRFS module added to kernel:

```
lsmod | grep mrfs
```

Check kernel log:

```
dmesg | tail
```

or

```
dmesg | less
```

## Running the tests

There are no tests for now :(

## Development

### Coding style

Stick to Linux Kernel coding style (https://www.kernel.org/doc/Documentation/CodingStyle) whenever possible


## Contributing

## Versioning

## Authors

## License



# Note

It doesn’t store any data yet, so it’s simple: touch image
We should also create a catalogue, which will be an assembling point (root) of our file system: mkdir dir
Now using this image we’ll assemble our file system: sudo mount -o loop -t mrfs ./image ./dir
If the operation ended successfully, we’ll see messages from our module in the system log. In order to disassemble the file system we should: sudo umount ./dir
Check the system log again

